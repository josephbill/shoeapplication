package com.example.shoeapp


class ProductModel (val id: String ,
                    val shoeName: String, val shoePrice: String,
                    val shoeContact: String, val shoePhone: String,
                    val shoeImage: String) {

    //blank constructor for reading process
    constructor() : this("","","","",
                         "","")

}