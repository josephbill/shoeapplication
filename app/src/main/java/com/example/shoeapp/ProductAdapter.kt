package com.example.shoeapp

import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.custom_item.view.*

class ProductAdapter(private val context: Context,
                     private val productModel: List<ProductModel> )
    : RecyclerView.Adapter<ProductAdapter.ProductViewHolder>()
{
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductAdapter.ProductViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.custom_item, parent, false)
        return ProductViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ProductAdapter.ProductViewHolder, position: Int) {
        //current position of each item
        val currentShoe  = productModel[position]
        //bind
        holder.productName.text  = "Shoe Brand: "  + currentShoe.shoeName
        holder.productPrice.text  = "Price:  " +  currentShoe.shoePrice
        holder.productPhone.text = "Contact Phone " +  currentShoe.shoePhone

        Glide.with(context)
                .load(currentShoe.shoeImage)
                .into(holder.productImage)

        //variables to store the items data
        val id  = currentShoe.id
        val price = currentShoe.shoePrice
        val phone = currentShoe.shoePhone
        val prodname  = currentShoe.shoeName
        val imagepath = currentShoe.shoeImage
        val contactname = currentShoe.shoeContact

        holder.itemclick.setOnClickListener {
            updateandDeleteDialog(id,price,phone,prodname,imagepath,contactname)
        }


    }

    private fun updateandDeleteDialog(id: String, price: String, phone: String,
                                      prodname: String, imagepath: String,
                                      contactname: String) {
        //raising a dialog
        val dialogBuilder = AlertDialog.Builder(context)
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)
                as LayoutInflater
        //attaching the dialog interface
        val dialogView = inflater.inflate(R.layout.updatedeletedialog, null)
        dialogBuilder.setView(dialogView)
        //view identification
        var updatePrice = dialogView.findViewById<TextInputEditText>(R.id.editPrice)
        var btnDelete = dialogView.findViewById<Button>(R.id.btnDelete)
        var btnUpdate = dialogView.findViewById<Button>(R.id.btnUpdate)
        //customize the dialog box
        dialogBuilder.setTitle("Update or Delete a Product")
        dialogBuilder.setIcon(R.drawable.ic_baseline_format_indent_increase_24)
        //create and show
        val dialog = dialogBuilder.create()
        dialog.show()

        btnUpdate.setOnClickListener {
            var priceEntry = updatePrice.text.toString()
            if (priceEntry.trim() != null){
                updateProductToFirebase(id,prodname,imagepath,priceEntry,phone,contactname)
            } else {
                Toast.makeText(context,"Fill the price first",Toast.LENGTH_LONG).show()

            }
        }

        btnDelete.setOnClickListener {
            deleteProductFromFirebase(id)
            dialog.dismiss()
            Toast.makeText(context,"Delete success",Toast.LENGTH_LONG).show()

        }
    }

    private fun deleteProductFromFirebase(id: String){
        //create ref to the db
        val databaseReference = FirebaseDatabase.getInstance()
                               .getReference("shoesTable").child(id)
        databaseReference.removeValue()
        Toast.makeText(context,"Shoe deleted Successfully",Toast.LENGTH_LONG).show()

    }


    private fun updateProductToFirebase(id: String, prodname: String, imagepath: String
                                        , priceEntry: String, phone: String, contactname: String) {
//create ref to the db
        val databaseReference = FirebaseDatabase.getInstance()
                .getReference("shoesTable").child(id)
        //take the data to model
        val shoeUpload = ProductModel(id,prodname,priceEntry,contactname,phone,imagepath)
        databaseReference.setValue(shoeUpload)
        Toast.makeText(context,"Update Successful",Toast.LENGTH_LONG).show()
    }

    override fun getItemCount() = productModel.size

    class ProductViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        //ref's views by id
        var productImage = itemView.image_view
        var productName = itemView.text_view_1
        var productPrice = itemView.text_view_2
        var productPhone = itemView.text_view_3
        var itemclick = itemView.cardClick
    }









}