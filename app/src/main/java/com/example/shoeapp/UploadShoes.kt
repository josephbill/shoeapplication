package com.example.shoeapp

import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.widget.Toast
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import kotlinx.android.synthetic.main.activity_upload_shoes.*
import java.io.IOException
import java.util.*

class UploadShoes : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    private val PICK_IMAGE_REQUEST = 71
    lateinit var productImage: Uri
    var productName: String = ""
    var productPrice: String = ""
    var productContactName: String = ""
    var productContactPhone : String = ""
    // tags for db and storage ref
    private var firebaseStorage: FirebaseStorage? = null
    private var storageRef: StorageReference? = null
    private var databaseRef: DatabaseReference? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_upload_shoes)

        auth   = FirebaseAuth.getInstance()
        //intializing storage and db ref
        firebaseStorage = FirebaseStorage.getInstance()
        storageRef = firebaseStorage!!.reference
        databaseRef = FirebaseDatabase.getInstance().getReference("shoesTable")

        btnSignOut.setOnClickListener {
            auth.signOut()
            updateUi()
        }

        btnView.setOnClickListener {
            val intent  = Intent(applicationContext,DisplayProducts::class.java)
            startActivity(intent)
        }

        editProductImage.setOnClickListener {
            val intent  = Intent()
            intent.type = "image/*"
            intent.action = Intent.ACTION_GET_CONTENT
            startActivityForResult(
                    Intent.createChooser(intent, "Select Picture"),
                   PICK_IMAGE_REQUEST
            )
        }

        btnUpload.setOnClickListener {
            productName = editShoe.text.toString()
            productPrice = editProductPrice.text.toString()
            productContactName = editContactName.text.toString()
            productContactPhone = editContactPhone.text.toString()
            //validation
            if (productName.isEmpty() && productPrice.isEmpty()
                    && productContactName.isEmpty() && productContactPhone.isEmpty()){
                Toast.makeText(
                        applicationContext,
                        " Fields should not be empty",
                        Toast.LENGTH_LONG
                ).show()
            } else {
                   submitToFirebase(productName,productContactPhone,productContactName
                           ,productPrice,productImage)
            }
        }

    }



    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK){
            //check if image was picked
            if (data == null || data.data == null){
                return
            }
            productImage = data.data!!
            //showing user their selection
            try {
                val bitmap  = MediaStore.Images.Media.getBitmap(contentResolver,
                        productImage)
                editProductImage.setImageBitmap(bitmap)
            }catch (e: IOException){
                e.printStackTrace()
            }
        }
    }

    private fun submitToFirebase(productName: String, productContactPhone: String,
                                 productContactName: String, productPrice: String
                                 , productImage: Uri) {

        //process to take data to firebase
        //UUID : refers to image name
        val ref = storageRef?.child("shoesImages/"
                + UUID.randomUUID().toString())
        //put the image to storage bucket
        val uploadTask = ref?.putFile(productImage!!)
        //monitoring the process
        val urlTask = uploadTask?.continueWithTask(
                Continuation<UploadTask.TaskSnapshot, Task<Uri>>
        {
            //checking if it failed
            if (!it.isSuccessful){
                it.exception?.let {
                    throw it
                }
            }
            return@Continuation ref.downloadUrl
        })?.addOnCompleteListener {
            if (it.isSuccessful) {
               //checking if upload process is complete : if complete we save the
                //download url for image
                val downloadUri = it.result
                Log.d("image","downlaod url " + downloadUri.toString())
                //end of storage process
                //beginning the process of taking data to realtime database
                //generate unique id for records
                val shoeId = databaseRef?.push()?.key
                //hold this data in our model
                val shoeUpload = shoeId.let {
                    ProductModel(shoeId.toString(),productName,productPrice
                            ,productContactName,productContactPhone,downloadUri.toString())
                }
                //send values to realtime databse
                if (shoeId != null){
                    databaseRef?.child(shoeId)?.setValue(shoeUpload)
                            ?.addOnCompleteListener {
                                Toast.makeText(
                                        applicationContext,
                                        "Shoe added successfully",
                                        Toast.LENGTH_LONG
                                ).show()
                            }?.addOnFailureListener{
                                Toast.makeText(
                                        applicationContext,
                                        "Error, check internet settings",
                                        Toast.LENGTH_LONG
                                ).show()
                            }
                }  //end of sending text data to real time database
            } else {
                // Handle failures //e.g //
                Toast.makeText(
                        applicationContext,
                        " Error occurred , check internet connection",
                        Toast.LENGTH_LONG
                ).show()
            }
        }?.addOnFailureListener {
            //here u can get actual error from firebase
            val messageError = it.message
            Toast.makeText(applicationContext,
                    " Error is " + messageError, Toast.LENGTH_LONG)
                    .show()
        }
    }

    private fun updateUi() {
        val intent = Intent(applicationContext,HomeActivity::class.java)
        startActivity(intent)
    }
}









