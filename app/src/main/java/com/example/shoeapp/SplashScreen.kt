package com.example.shoeapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.WindowManager

class SplashScreen : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
       //using handler to delY SCREEN
        Handler().postDelayed({
            val intents = Intent(this, OnboardingScreen::class.java)
            startActivity(intents)
        }, 3000) //screen delays for 3 seconds

    }
}